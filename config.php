<?php
// HTTP
define('HTTP_SERVER', 'http://octest.fotopanda.lt/');

// HTTPS
define('HTTPS_SERVER', 'http://octest.fotopanda.lt/');

// DIR
define('DIR_APPLICATION', '/home/fotopand/domains/fotopanda.lt/public_html/octest/catalog/');
define('DIR_SYSTEM', '/home/fotopand/domains/fotopanda.lt/public_html/octest/system/');
define('DIR_IMAGE', '/home/fotopand/domains/fotopanda.lt/public_html/octest/image/');
define('DIR_STORAGE', '/home/fotopand/domains/fotopanda.lt/public_html/storage/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/theme/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'fotopand_octest');
define('DB_PASSWORD', 'LeitariaiDurduliuoti');
define('DB_DATABASE', 'fotopand_octest');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');